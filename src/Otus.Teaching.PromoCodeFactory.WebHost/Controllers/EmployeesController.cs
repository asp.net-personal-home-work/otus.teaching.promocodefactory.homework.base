﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles?.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Добавить нового сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<EmployeeShortResponse>> CreateEmployeeAsync(CreateEmployeeRequest employeeVm)
        {
            var employee = await _employeeRepository.CreateAsync(new Employee
            {
                FirstName = employeeVm.FirstName,
                LastName = employeeVm.LastName,
                Email = employeeVm.Email,
            });
          

            return new EmployeeShortResponse
            {
                Id = employee.Id,
                FullName = employee.FullName,
                Email = employee.Email
            };
        }

        /// <summary>
        /// Изменить сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateEmployeeAsync(UpdateEmployeeRequest updateEmployeeRequest)
        {
            if (updateEmployeeRequest.Id is null)
            {
                return BadRequest();
            }

            var employee = await _employeeRepository.GetByIdAsync(updateEmployeeRequest.Id.Value);

            if (employee is null)
            {
                return NotFound();
            }

            await _employeeRepository.UpdateAsync(new Employee
            {
                Id = updateEmployeeRequest.Id.Value,
                FirstName = updateEmployeeRequest.FirstName,
                LastName = updateEmployeeRequest.LastName,
                Email = updateEmployeeRequest.Email
            });

            return Ok();
        }

        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteEmployeeAsync(Guid? id)
        {
            if (id is null)
            {
                return BadRequest();
            }

            var employee = await _employeeRepository.GetByIdAsync(id.Value);

            if (employee is null)
            {
                return NotFound();
            }

            await _employeeRepository.DeleteAsync(employee);

            return Ok();
        }
    }
}